# Pe インストーラ

[![Build status](https://ci.appveyor.com/api/projects/status/r8ulo660vyp8lydj/branch/master?svg=true)](https://ci.appveyor.com/project/sk_0520/pe-installer/branch/master)


* [ダウンロード](https://bitbucket.org/sk_0520/pe.installer/downloads/Pe.Installer.exe)
* 本インストーラは[Pe](https://bitbucket.org/sk_0520/pe/)を自動インストールします


## 動作環境

* .NET Framework 4.8
* インターネット接続可能

## ライセンス

* [WTFPL 2](sam.zoy.org/wtfpl/COPYING)


## ライブラリ

* [ILMerge](https://github.com/dotnet/ILMerge)
* [Newtonsoft.Json](https://www.newtonsoft.com/json)
* [SevenZipExtractor](https://github.com/adoconnection/SevenZipExtractor)

